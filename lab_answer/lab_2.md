1. **Apakah perbedaan antara JSON dan XML?**

   JSON ( JavaScript Object Notation) adalah data-interchange format yang sebenarnya language-independent, tetapi dibuat berdasarkan JavaScript, di mana tujuan utamanya adalah merepresentasikan object. XLM (Extensible Markup Language) adalah bahasa yang menyederhanakan proses peyimpanan dan pengiriman data antar server. XML didesain untuk mengirim data, bukan untuk menampilkannya. Karena itu, XML mendukung berbagai bentuk encoding sedangkan JSON hanya mendukung UTF-8.

2. **Apakah perbedaan antara HTML dan XML?**

   Walaupun keduanya sama-sama markup language, HTML didesain untuk menampilkan data sedangkan XML untuk mengirim data. HTML bersifat static dan XML dinamis. XML memberikan  framework untuk bermacam-macam markup languages.

from django.db import models

# Create your models here.

class Note(models.Model):
	to = models.CharField(max_length=30)
	from_user = models.CharField(max_length=30)
	title = models.CharField(max_length=50)
	message = models.TextField()

	def __str__(self):
		return self.title
from django.shortcuts import render, redirect
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url ='/admin/login/')
def index(request):
    friends = Friend.objects.all  
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url ='/admin/login/')
def add_friend(request):
    context = dict()
    form = FriendForm(request.POST or None)

    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('/lab-3/')

    context["form"] = form
    return render(request, "lab3_form.html", context)